import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Slider
} from 'react-native';
import { Provider, connect } from 'react-redux';

import MainNavigator from './src/navigation';
import store from './src/store';

export default class App extends Component<{}> {

  render() {
    return (
      <Provider store={store}>
         <MainNavigator />
      </Provider>
    );
  }
}
