import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  StatusBar,
  Text,
  View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
//import { Icon } from 'react-native-elements';
import { Provider } from 'react-redux';

//import Colors from '../constants/Colors';

import SearchScreen from '../screens/SearchScreen';
import ResultScreen from '../screens/ResultScreen';

import store from '../store';
import { connect } from 'react-redux';
//import getDefaultTab from '../actions/ModifySettingsActions';



const MainNavigator = StackNavigator({
    Search: {
      screen: SearchScreen
    },
    Result: {
      screen: ResultScreen
    }

})

export default MainNavigator;
